package org.evolvis.java.web;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * A specialized HttpServletRequestWrapper that protects the wrapped request against
 * changing the character encoding anytime later.
 * 
 * @author Tino Rink <t.rink@tarent.de>
 */
public class StaticCharEncodingServletRequest extends HttpServletRequestWrapper {

	private final String charsetEncoding;
	
	/**
	 * @param request that should be wrapped
	 * @param charsetEncoding that should be set and protected
	 * @throws UnsupportedEncodingException thrown if charset encoding is invalid / not supported
	 */
	public StaticCharEncodingServletRequest(HttpServletRequest request, String charsetEncoding) 
			throws UnsupportedEncodingException {
		super(request);
		request.setCharacterEncoding(charsetEncoding);
		this.charsetEncoding = charsetEncoding;
	}
	
	@Override
	public String getCharacterEncoding() {
		return this.charsetEncoding;
	}
	
	@Override
	public void setCharacterEncoding(String enc) {
		// do nothing
	}
}
