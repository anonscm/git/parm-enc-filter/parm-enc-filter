package org.evolvis.java.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.evolvis.config.ConfigFactory;
import org.evolvis.config.Configuration;

/**
 * ParameterEncodingFilter supports the setting of the encoding for the transmitted 
 * request parameter based on a magic GET-Request parameter.
 * 
 * @author Tino Rink <t.rink@tarent.de>
 */
public class ParameterEncodingFilter implements Filter {

	public static final String FILTER_ENABLED = "parameter-encoding-filter.enabled";

	@Deprecated
	public static final String FEATURE_ENABLED = FILTER_ENABLED;
	
	public static final String CHARSET_PARAMETER_NAME = "parameter-encoding-filter.parameter_name";

	public static final String FEATURE_WRAPPING_ENABLED = "parameter-encoding-filter.wrapping.enabled";
	
	public static final String DEFAULT_CHARSET_PARAMETER = "_charset_";
	
	private boolean isFilterEnabled;
	
	private boolean isWrappingEnabled;
	
	private String charsetParameter;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		final Configuration config = ConfigFactory.getConfiguration();
		isFilterEnabled = config.getAsBoolean(FILTER_ENABLED, false);
		isWrappingEnabled = config.getAsBoolean(FEATURE_WRAPPING_ENABLED, false);
		charsetParameter = config.get(CHARSET_PARAMETER_NAME, DEFAULT_CHARSET_PARAMETER) + "=";
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		ServletRequest maybeWrappedRequest = request;
		
		if (isFilterEnabled && request instanceof HttpServletRequest) {
			
			final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			final String queryString = httpServletRequest.getQueryString();
			
			String charset = null;
			
			if (queryString != null) {
				charset = findCharset(queryString.toLowerCase());
			}
			
			if (charset != null) {
				
				if (isWrappingEnabled) {
					maybeWrappedRequest = new StaticCharEncodingServletRequest(httpServletRequest, charset);
				}
				else {
					httpServletRequest.setCharacterEncoding(charset);
				}
				
			}
		}
		
		chain.doFilter(maybeWrappedRequest, response);
	}

	@Override
	public void destroy() {
		// do nothing
	}

	/**
	 * Searchs in the given query String for the magic GET-Parameter that defines 
	 * a specific character encoding for this request processing.
	 * 
	 * @return The found character encoding or null
	 */
	String findCharset(final String queryString) {
		
		final int charsetParamStartIdx = queryString.indexOf(charsetParameter);
		
		if (charsetParamStartIdx > -1) {

			int charsetParamEndIdx = queryString.indexOf('&', charsetParamStartIdx);
			
			if (charsetParamEndIdx == -1) {
				charsetParamEndIdx = queryString.length();
			}
			
			if (charsetParamEndIdx - (charsetParamStartIdx + charsetParameter.length()) > 1) {
				return queryString.substring(charsetParamStartIdx + charsetParameter.length(), charsetParamEndIdx);				
			}
		}
		
		return null;
	}
}
