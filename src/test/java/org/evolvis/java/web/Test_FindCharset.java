package org.evolvis.java.web;

import static org.junit.Assert.*;

import javax.servlet.ServletException;

import org.evolvis.config.PropertyConfig;
import org.junit.Before;
import org.junit.Test;

/**
 * Test the different test case of the {@link ParameterEncodingFilter#findCharset(String)} method
 *  
 * @author Tino Rink <t.rink@tarent.de>
 */
public class Test_FindCharset extends AbstractBasicTestSetup {

	private ParameterEncodingFilter filter;
	
	@Before
	public void setUp() throws ServletException {
    	
		System.setProperty(PropertyConfig.TARENT_CONFIG_KEY, "target/test-classes/default");
		
    	filter = new ParameterEncodingFilter();
		filter.init(null);
	}
	
	@Test
	public void empty_Value_Minimal_Test() {
		final String queryString = ParameterEncodingFilter.DEFAULT_CHARSET_PARAMETER + "=";
		assertNull(filter.findCharset(queryString));
	}
	
	@Test
	public void utf8_Minimal_Test() {
		final String queryString = ParameterEncodingFilter.DEFAULT_CHARSET_PARAMETER + "=utf8";
		assertEquals("utf8", filter.findCharset(queryString));
	}
	
	@Test
	public void utf8_Minimal_Amp_Test() {
		final String queryString = ParameterEncodingFilter.DEFAULT_CHARSET_PARAMETER + "=utf8&";
		assertEquals("utf8", filter.findCharset(queryString));
	}
	
	@Test
	public void utf8_Url_Test() {
		final String queryString = "http://test.de/test?" + ParameterEncodingFilter.DEFAULT_CHARSET_PARAMETER + "=utf8";
		assertEquals("utf8", filter.findCharset(queryString));
	}
	
	@Test
	public void utf8_Url_Amp_Test() {
		final String queryString = "http://test.de/test?" + ParameterEncodingFilter.DEFAULT_CHARSET_PARAMETER + "=utf8&";
		assertEquals("utf8", filter.findCharset(queryString));
	}
	
	@Test
	public void invalid_Param_1_Test() {
		final String queryString = ParameterEncodingFilter.DEFAULT_CHARSET_PARAMETER;
		assertNull(filter.findCharset(queryString));
	}
	
	@Test
	public void invalid_Param_2_Test() {
		final String queryString = ParameterEncodingFilter.DEFAULT_CHARSET_PARAMETER + "utf8&";
		assertNull(filter.findCharset(queryString));
	}
}
