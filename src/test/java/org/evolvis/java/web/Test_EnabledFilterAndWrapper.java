package org.evolvis.java.web;

import static org.mockito.Mockito.*;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.evolvis.config.PropertyConfig;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Test the behavior of the ParameterEncodingFilter with enabled filter 
 * and wrapper options (see corresponding file: src/test/resources/enabledFilterAndWrapper)
 * 
 * @author Tino Rink <t.rink@tarent.de>
 */
public class Test_EnabledFilterAndWrapper extends AbstractBasicTestSetup {

	private ParameterEncodingFilter filter;
	
	private FilterChain filterChainMock;
	private HttpServletRequest httpServletRequestMock;
	private HttpServletResponse httpServletResponseMock;
	
	@Before
	public void setup() throws ServletException {
		
    	System.setProperty(PropertyConfig.TARENT_CONFIG_KEY, "target/test-classes/enabledFilterAndWrapper");
		
    	filter = mock(ParameterEncodingFilter.class, Mockito.CALLS_REAL_METHODS);
		filter.init(null);
		
		filterChainMock = mock(FilterChain.class);
		httpServletRequestMock = mock(HttpServletRequest.class);
		httpServletResponseMock = mock(HttpServletResponse.class);
	}

	/**
	 * Test, that the processing of HttpServletRequest with enabled Filter, Wrapper and without charset query parameter 
	 * results in following behavior:
	 * must not call: {@link ParameterEncodingFilter#findCharset()}, {@link ServletRequest#setCharacterEncoding(String)}
	 * and must call: {@link FilterChain#doFilter(HttpServletRequest, HttpServletResponse)}
	 */
	@Test
	public void testCallingFilterchainForHttpServletRequest() throws IOException, ServletException {
		filter.doFilter(httpServletRequestMock, httpServletResponseMock, filterChainMock);
		
		verify(filter, times(0)).findCharset(anyString());
		
		verify(httpServletRequestMock, times(0)).setCharacterEncoding(anyString());

		verify(filterChainMock, times(1)).doFilter(eq(httpServletRequestMock), eq(httpServletResponseMock));
	}

	/**
	 * Test, that the processing of HttpServletRequest with enabled Filter, Wrapper and with a valid charset query parameter 
	 * results in following behavior:
	 * must call: {@link ParameterEncodingFilter#findCharset()}, {@link ServletRequest#setCharacterEncoding(String)},
	 * {@link FilterChain#doFilter(StaticCharEncodingServletRequest, HttpServletResponse)}
	 */
	@Test
	public void testCallingFilterchainForHttpServletRequestWithQueryParm() throws IOException, ServletException {
	
		when(httpServletRequestMock.getQueryString()).thenReturn(ParameterEncodingFilter.DEFAULT_CHARSET_PARAMETER + "=utf8");
		
		filter.doFilter(httpServletRequestMock, httpServletResponseMock, filterChainMock);
		
		verify(filter, times(1)).findCharset(anyString());
		
		verify(httpServletRequestMock, times(1)).setCharacterEncoding(anyString());
		
		verify(filterChainMock, times(1)).doFilter(isA(StaticCharEncodingServletRequest.class), eq(httpServletResponseMock));
	}
}
