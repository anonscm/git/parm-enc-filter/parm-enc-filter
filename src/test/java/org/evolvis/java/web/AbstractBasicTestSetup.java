package org.evolvis.java.web;

import org.evolvis.config.ConfigFactory;
import org.evolvis.config.PropertyConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class AbstractBasicTestSetup {

	@BeforeClass
	public static void basicSetup() {
		System.getProperties().remove(ConfigFactory.CONFIGURATION_IMPL);
		System.getProperties().remove(PropertyConfig.TARENT_CONFIG_KEY);
	}
	
	@AfterClass
	public static void basicCleanup() {
		System.getProperties().remove(ConfigFactory.CONFIGURATION_IMPL);
		System.getProperties().remove(PropertyConfig.TARENT_CONFIG_KEY);
	}
}
