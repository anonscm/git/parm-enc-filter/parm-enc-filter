package org.evolvis.java.web;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.UnsupportedEncodingException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;

/**
 * Test the behavior of the class {@link StaticCharEncodingServletRequest}
 *  
 * @author Tino Rink <t.rink@tarent.de>
 */
public class Test_StaticCharEncServletReq extends AbstractBasicTestSetup {
	
	private HttpServletRequest httpServletRequestMock;
	
	private String testCharset = "utf8";
	
	@Before
	public void setup() throws ServletException {
		httpServletRequestMock = mock(HttpServletRequest.class);
	}

	@Test
	public void staticCharset() throws UnsupportedEncodingException {
		
		final StaticCharEncodingServletRequest request = new StaticCharEncodingServletRequest(httpServletRequestMock, "utf8");
		
		assertEquals(testCharset, request.getCharacterEncoding());
		
		request.setCharacterEncoding("foobar");
		
		assertEquals(testCharset, request.getCharacterEncoding());

		verify(httpServletRequestMock, times(1)).setCharacterEncoding(eq(testCharset));
	}
}
